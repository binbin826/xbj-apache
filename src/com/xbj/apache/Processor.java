package com.xbj.apache;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;

public class Processor extends Thread{

	private Socket socket;
	private InputStream in;
	private PrintStream out;
	
	
	public Processor(Socket socket){
		this.socket = socket;
		try {
			in = socket.getInputStream();
			out = new PrintStream(socket.getOutputStream());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void run() {
		String fileName = parse(in);
		sendFile(fileName);
	}

	private void sendFile(String fileName) {
		File file = new File(Commons.WEB_ROOT+fileName);
		if(!file.exists()){
			sendErrorMessage(404,"File Not Fount");
			return;
		}
		try{
			InputStream inputStream = new FileInputStream(file);
			byte[] content = new byte[(int)file.length()];
			inputStream.read(content);
			out.println("HTTP/1.0 200 queryFile");
            out.println("content-lenght: " + content.length);
            out.println();
            out.write(content);
            out.flush();
            out.close();
            in.close();
		}catch(FileNotFoundException e){
			e.printStackTrace();
		}catch(IOException e){
			e.printStackTrace();
		}
		
	}

	private String parse(InputStream in) {
		BufferedReader br = new BufferedReader(new InputStreamReader(in));
		String fileName = "";
		try{
			String httpMessage = br.readLine();
			String[] content = httpMessage.split(" ");
			if(content.length != 3){
				sendErrorMessage(400,"client query error");
				return null;
			}
			fileName = content[1];
		}catch(IOException e){
			e.printStackTrace();
		}
		return fileName;
	}

	private void sendErrorMessage(int errorCode, String errorMessage) {
		out.println("HTTP/1.0 " + errorCode + " " + errorMessage);
        out.println("content-type: text/html");
        out.println();
        out.println("<html>");
        out.println("<title>Error Message</title>");
        out.println("<body>");
        out.println("<h1>ErrorCode:"+errorCode+",ErrorMessage:"+errorMessage+"</h1>");
        out.println("</body>");
        out.println("</html>");
        out.flush();
        out.close();
        try {
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
	}
	
}
