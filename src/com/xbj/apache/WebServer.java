package com.xbj.apache;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class WebServer {

	private final static List<Processor> processors = new ArrayList<Processor>();
	
	public static void main(String args[]) throws IOException{
		ServerSocket serverSocket = new ServerSocket(8888);
		System.out.println("服务器已经打开8888端口，开始准备接受数据......");
		try{
			while(true){
				Socket socket = serverSocket.accept();
				Processor processor = new Processor(socket);
				processors.add(processor);
				System.out.println("socket_thread:"+processor.getName()+"开始启动");
			}
		}finally{
			serverSocket.close();
			interruptProcessors();
		}
	}

	private static void interruptProcessors() {
		for(Processor processor:processors){
			processor.interrupt();
		}
		
	}
}
